import requests
import pytest

s = "Alibaba"
url = "https://the-internet.herokuapp.com/context_menu"
response = requests.get(url)

# test should FAIL if string "Alibaba" is not found on the same page
def test():
    assert (s in response.text), "Alibaba is not found on the same page"
