import requests
import pytest

s = "Right-click in the box below to see one called 'the-internet'"
url = "https://the-internet.herokuapp.com/context_menu"
response = requests.get(url)

# test should PASS if the string Right-click in the box below to see one called 'the-internet' found on page
def test():
    assert (s in response.text), "Right-click in the box below to see one called 'the-internet' not found on page"
